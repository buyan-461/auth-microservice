<?php

use src\App;

require "config/bootstrap.php";

try {
    $app = new App();
    echo $app->run();
} catch (Exception $e) {
    echo $e->getMessage();
}