<?php

namespace src;

use src\Core\Container\Container;
use src\Core\Web\Router;

class App
{
    /** @var Container */
    private ?Container $container = null;

    /** @var Router */
    private Router $router;

    public function __construct()
    {
        $this->container = new Container();
        $this->router = $this->container->get(Router::class);
    }

    public function run()
    {
        $controller = $this->router->getController();
        $action = $this->router->getAction();
        return ($this->container->get($controller))->$action();
    }
}