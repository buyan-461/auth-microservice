<?php

namespace src\Core\Web;

class Router
{
    /** @var array */
    private array $config = [];

    /** @var Request */
    private Request $request;

    /** @var string */
    private string $controller;

    /** @var string */
    private string $action;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->config = require CONFIG . '/url-manager.php';
        $this->init();
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @throws
     */
    private function init()
    {
        if (!isset($this->config[$this->request->getUri()])) {
            throw new \Exception('Routing error');
        }
        $path = $this->config[$this->request->getUri()];
        $params = explode('/', $path);
        if (count($params) < 2) {
            throw new \Exception('Url config error.');
        }
        $controller = 'src\Module\\' . ucfirst($params[0]) . "\\" . ucfirst($params[0]) . 'Controller';
        if (!class_exists($controller)) {
            throw new \Exception('Controller not found');
        }
        $this->controller = $controller;
        $this->action = $params[1];
    }
}