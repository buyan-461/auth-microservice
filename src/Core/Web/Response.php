<?php

namespace src\Core\Web;

use src\Core\Logger\MessageLogger;

class Response
{
    public function send()
    {
        $messages = MessageLogger::getSuccessMessages();
        foreach ($messages as $message) {
            echo $message . PHP_EOL;
        }
    }
}