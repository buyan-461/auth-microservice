<?php

namespace src\Core\Repository;

use src\Core\Db\Query;

abstract class AbstractRepository
{
    /**
     * @param array $params
     * @return bool
     */
    public function save(array $params): bool
    {
        return (new Query())->insert($this->getTableName(), $params);
    }

    /**
     * @return string
     */
    abstract protected function getTableName(): string;
}