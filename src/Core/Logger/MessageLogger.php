<?php

namespace src\Core\Logger;

class MessageLogger
{
    /** @var string[] */
    private static array $success = [];

    /**
     * @param string $message
     */
    public static function success(string $message): void
    {
        static::$success[] = $message;
    }

    /**
     * @return string[]
     */
    public static function getSuccessMessages(): array
    {
        return static::$success;
    }
}