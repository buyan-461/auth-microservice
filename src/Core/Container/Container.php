<?php

namespace src\Core\Container;

use Exception;
use ReflectionClass;
use ReflectionParameter;

class Container
{
    /** @var array */
    private array $instances = [];

    /**
     * @param string $abstract
     * @param string|null $concrete
     */
    public function set(string $abstract, string $concrete = null): void
    {
        $this->instances[$abstract] = $concrete ?? $abstract;
    }

    /**
     * @param string $abstract
     * @return mixed|object|null
     * @throws Exception
     */
    public function get(string $abstract)
    {
        if (!isset($this->instances[$abstract])) {
            $this->set($abstract);
        }
        return $this->resolve($this->instances[$abstract]);
    }

    /**
     * @param string $concrete
     * @return mixed|null|object
     * @throws
     */
    private function resolve(string $concrete)
    {
        $reflection = new ReflectionClass($concrete);
        if (!$reflection->isInstantiable()) {
            throw new Exception("Class $concrete can not be instantiable.");
        }
        $constructor = $reflection->getConstructor();
        if (is_null($constructor)) {
            return $reflection->newInstance();
        }
        $params = $constructor->getParameters();
        $dependencies = $this->getDependencies($params);

        return $reflection->newInstanceArgs($dependencies);
    }

    /**
     * @param ReflectionParameter[] $params
     * @return array
     * @throws
     */
    private function getDependencies(array $params): array
    {
        $dependencies = [];
        foreach ($params as $param) {
            $dependency = $param->getClass();
            if ($dependency) {
                $dependencies[] = $this->get($dependency->name);
                continue;
            }
            if ($param->isDefaultValueAvailable()) {
                $dependencies[] = $param->getDefaultValue();
                continue;
            }
            throw new Exception("Can not resolve dependency $param->name.");
        }
        return $dependencies;
    }
}