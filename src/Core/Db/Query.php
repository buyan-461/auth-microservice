<?php

namespace src\Core\Db;

use PDOStatement;

class Query
{
    /**
     * @param string $tableName
     * @param array $condition
     * @return mixed
     */
    public function queryWhere(string $tableName, array $condition)
    {
        $sql = "SELECT * FROM public.$tableName ";
        if (!$condition) {
            return $this->query($sql);
        }

        $sql .= " WHERE ";
        $newCondition = [];
        foreach ($condition as $attr => $value) {
            $newCondition[] = " $attr='$value' ";
        }
        $sql .= implode(" AND ", $newCondition);
        return $this->query($sql);
    }

    /**
     * @param string $tableName
     * @param array $params
     * @return bool
     */
    public function insert(string $tableName, array $params): bool
    {
        $attrs = implode(",", array_keys($params));
        $values = implode("','", array_values($params));
        $sql = "INSERT INTO \"$tableName\" ($attrs) VALUES ('$values');";

        return $this->execute($sql);
    }

    /**
     * @param string $query
     * @return bool
     */
    private function execute(string $query): bool
    {
        return !!Db::getConnection()->exec($query);
    }

    /**
     * @param string $query
     * @return mixed
     */
    private function query(string $query)
    {
        $statement = Db::getConnection()->query($query . ';');
        if ($statement instanceof PDOStatement) {
            $object = $statement->fetchObject();
            if ($object) {
                return $object;
            }
        }
        return null;
    }
}