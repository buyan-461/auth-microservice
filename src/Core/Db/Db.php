<?php

namespace src\Core\Db;

use PDO;

class Db
{
    /** @var PDO */
    private static ?PDO $connection = null;

    /**
     * @return PDO
     */
    public static function getConnection(): PDO
    {
        if (!static::$connection) {
            static::setConnection();
        }
        return static::$connection;
    }

    private static function setConnection(): void
    {
        $name = getenv('DB_NAME');
        $user = getenv('DB_USER');
        $password = getenv('DB_PASSWORD');
        $host = getenv('DB_HOST');
        $dns = "pgsql:dbname=" . $name . ";host=" . $host;

        static::$connection = new PDO($dns, $user, $password);
    }
}