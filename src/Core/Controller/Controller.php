<?php

namespace src\Core\Controller;

use Exception;
use src\Core\Container\Container;
use src\Core\Web\Request;
use src\Core\Web\Response;

class Controller
{
    /** @var Container */
    protected ?Container $container = null;

    /** @var Request */
    protected ?Request $request = null;

    /** @var Response */
    protected ?Response $response = null;

    public function __construct()
    {
        $this->container = new Container();
        $this->request = $this->container->get(Request::class);
        $this->response = $this->container->get(Response::class);
    }

    /**
     * @param string $view
     * @param array $params
     * @return string
     * @throws Exception
     */
    protected function render(string $view, array $params = []): string
    {
        ob_start();
        ob_implicit_flush(false);

        extract($params);
        $viewFile = $this->getViewFile($view);
        require $viewFile;
        $this->response->send();

        return ob_get_clean();
    }

    /**
     * @param string $url
     */
    protected function redirect(string $url)
    {
        header("Location: $url");
        exit();
    }

    /**
     * @param string $url
     */
    protected function refresh(string $url)
    {
        header("Refresh:1;url=$url");
        $this->response->send();
        exit();
    }

    /**
     * @param string $view
     * @return string
     * @throws Exception
     */
    private function getViewFile(string $view): string
    {
        $file = VIEW . "$view.php";
        if (!is_file($file)) {
            throw new Exception('View does not exists.');
        }
        return $file;
    }
}