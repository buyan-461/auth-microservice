<?php

namespace src\Core\Queue;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Connection
{
    /** @var AMQPStreamConnection */
    private AMQPStreamConnection $connection;

    /** @var QueueNameParser */
    private QueueNameParser $queueNameParser;

    public function __construct(AMQPStreamConnection $connection, QueueNameParser $queueNameParser)
    {
        $this->connection = $connection;
        $this->queueNameParser = $queueNameParser;
    }

    /**
     * @param string $message
     * @param string $topic
     * @throws
     */
    public function publish(string $message, string $topic)
    {
        $message = new AMQPMessage($message);

        $chanel = $this->connection->channel();
        $chanel->exchange_declare($topic, 'fanout', false, false, false);
        $chanel->basic_publish($message, $topic);

        $chanel->close();
        $this->connection->close();
    }

    /**
     * @param string|null $topic
     * @throws
     */
    public function listen(string $topic = null)
    {
        $chanel = $this->connection->channel();
        $chanel->exchange_declare($topic, 'fanout', false, false, false);
        list($queue_name) = $chanel->queue_declare("", false, false, true, false);

        $chanel->queue_bind($queue_name, $topic);
        $callback = $this->getCallback($topic);

        $chanel->basic_consume($queue_name, '', false, true, false, false, $callback);

        while ($chanel->is_consuming()) {
            $chanel->wait();
        }
    }

    /**
     * @param string $topic
     * @return callable
     */
    private function getCallback(string $topic): callable
    {
        $callback = function (AMQPMessage $message) use ($topic) {
            try {
                list($object, $method) = $this->queueNameParser->parse($topic);
                $methodParams = json_decode($message->body, true);

                return call_user_func_array([$object, $method], $methodParams);
            } catch (\Throwable $e) {
                echo $e->getMessage();
                return null;
            }
        };
        return $callback;
    }
}