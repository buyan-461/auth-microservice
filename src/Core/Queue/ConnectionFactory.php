<?php

namespace src\Core\Queue;

use Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class ConnectionFactory
{
    /** @var QueueNameParser */
    private QueueNameParser $queueNameParser;

    public function __construct(QueueNameParser $queueNameParser)
    {
        $this->queueNameParser = $queueNameParser;
    }

    /**
     * @return Connection
     * @throws
     */
    public function create(): Connection
    {
        //crutch for running rabbitmq consumer from docker
        while (!$connection = $this->getConnection()) {
            sleep(2);
        }

        if (!$connection->isConnected()) {
            throw new Exception('Queue connection failed.');
        }
        return new Connection($connection, $this->queueNameParser);
    }

    private function getConnection()
    {
        try {
            return new AMQPStreamConnection(
                getenv('RABBITMQ_HOST'),
                getenv('RABBITMQ_PORT'),
                getenv('RABBITMQ_USER'),
                getenv('RABBITMQ_PASSWORD')
            );
        } catch (\Throwable $e) {
            return null;
        }
    }
}