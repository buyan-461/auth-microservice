<?php

namespace src\Core\Queue;

class Receiver
{
    /** @var Connection */
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string|null $topic
     */
    public function listen(string $topic = null): void
    {
        $this->connection->listen($topic);
    }
}