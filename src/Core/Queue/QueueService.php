<?php

namespace src\Core\Queue;

class QueueService
{
    /** @var ConnectionFactory */
    private ConnectionFactory $connectionFactory;

    public function __construct(ConnectionFactory $connectionFactory)
    {
        $this->connectionFactory = $connectionFactory;
    }

    /**
     * @return Sender
     * @throws
     */
    public function getSender(): Sender
    {
        $connection = $this->connectionFactory->create();
        return new Sender($connection);
    }

    /**
     * @return Receiver
     * @throws
     */
    public function getReceiver(): Receiver
    {
        $connection = $this->connectionFactory->create();
        return new Receiver($connection);
    }
}