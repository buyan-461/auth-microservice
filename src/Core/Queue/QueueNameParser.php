<?php

namespace src\Core\Queue;

use Exception;
use src\Core\Container\Container;

class QueueNameParser
{
    /** @var Container */
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $queueName
     * @return array
     * @throws
     */
    public function parse(string $queueName): array
    {
        list($module, $className, $method) = explode('.', $queueName);
        $className = str_replace('_', '', ucwords($className, '_'));
        $class = "src\\Module\\" . ucfirst($module) . "\\" . $className;
        if (!class_exists($class)) {
            throw new \Exception("Class $class does not exists.");
        }
        $object = $this->container->get($class);
        if (!method_exists($object, $method)) {
            throw new Exception("Method $method of class $class does not exists.");
        }
        return [$object, $method];
    }
}