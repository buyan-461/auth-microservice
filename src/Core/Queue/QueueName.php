<?php

namespace src\Core\Queue;

class QueueName
{
    /** @var string */
    public const SEND_EMAIL = 'email.email_service.send';
}