<?php

namespace src\Core\Queue;

class Sender
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $data
     * @param string $topic
     */
    public function publish(array $data, string $topic)
    {
        $this->connection->publish(json_encode($data), $topic);
    }
}