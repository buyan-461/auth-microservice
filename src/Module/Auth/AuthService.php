<?php

namespace src\Module\Auth;

use Exception;
use src\Core\Logger\MessageLogger;
use src\Module\Email\EmailService;
use src\Module\User\UserService;

class AuthService
{
    /** @var UserService */
    private UserService $userService;

    /** @var EmailService */
    private EmailService $emailService;

    public function __construct(UserService $userService, EmailService $emailService)
    {
        $this->userService = $userService;
        $this->emailService = $emailService;
    }

    /**
     * @return bool
     */
    public function isLogin(): bool
    {
        return !!(isset($_SESSION['user_id']) && isset($_SESSION['username']));
    }

    /**
     * @param string $username
     * @param string $password
     * @return bool
     * @throws Exception
     */
    public function login(string $username, string $password): bool
    {
        $user = $this->userService->getUser($username, $password);
        $_SESSION['user_id'] = $user->id;
        $_SESSION['username'] = $user->username;
        return true;
    }

    public function logout(): void
    {
        $_SESSION = [];
    }

    /**
     * @param string $username
     * @param string $password
     * @param string $email
     * @return bool
     */
    public function register(string $username, string $password, string $email): bool
    {
        if ($this->userService->create($username, $password, $email)) {
            MessageLogger::success('Account created.');
            $this->emailService->sendQueue($email, 'Account created.');
            return true;
        }
        return false;
    }
}