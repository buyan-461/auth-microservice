<?php

namespace src\Module\Auth;

use src\Core\Controller\Controller;

class AuthController extends Controller
{
    /** @var AuthService */
    private AuthService $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
        parent::__construct();
    }

    /**
     * @return string
     * @throws
     */
    public function index()
    {
        if ($this->authService->isLogin()) {
            return $this->render('logout');
        }
        $username = $this->request->getPostParam('username');
        $password = $this->request->getPostParam('password');
        if ($username && $password) {
            if ($this->authService->login($username, $password)) {
                return $this->render('logout');
            }
        }
        return $this->render('login');
    }

    public function register()
    {
        $username = $this->request->getPostParam('username');
        $password = $this->request->getPostParam('password');
        $email = $this->request->getPostParam('email');
        if ($username && $password && $email) {
            if ($this->authService->register($username, $password, $email)) {
                $this->refresh('/');
            }
        }
        return $this->render('register');
    }

    /**
     * @throws
     */
    public function logout()
    {
        if (!$this->authService->isLogin()) {
            $this->redirect('/');
        }
        $this->authService->logout();
        $this->redirect('/');
    }
}