<?php

namespace src\Module\User;

use src\Core\Db\Query;
use src\Core\Repository\AbstractRepository;
use stdClass;

class UserRepository extends AbstractRepository
{
    /**
     * @param string $username
     * @param string $password
     * @return stdClass|null
     */
    public function findUser(string $username, string $password): ?stdClass
    {
        return (new Query())->queryWhere('user', ['username' => $username, 'password' => $password]);
    }

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'user';
    }
}