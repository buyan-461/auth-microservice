<?php

namespace src\Module\User;

use Exception;
use stdClass;

class UserService
{
    /** @var UserRepository */
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $username
     * @param string $password
     * @return stdClass
     * @throws
     */
    public function getUser(string $username, string $password): stdClass
    {
        $user = $this->userRepository->findUser($username, $password);
        if (!$user) {
            throw new Exception('User not found');
        }
        return $user;
    }

    /**
     * @param string $username
     * @param string $password
     * @param string $email
     * @return bool
     */
    public function create(string $username, string $password, string $email): bool
    {
        return $this->userRepository->save([
            'username' => $username,
            'password' => $password,
            'email' => $email
        ]);
    }
}