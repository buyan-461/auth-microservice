<?php

namespace src\Module\Email;

use src\Core\Logger\MessageLogger;
use src\Core\Queue\QueueName;
use src\Core\Queue\QueueService;

class EmailService
{
    /** @var Mailer */
    private Mailer $mailer;

    /** @var QueueService */
    private QueueService $queueService;

    public function __construct(Mailer $mailer, QueueService $queueService)
    {
        $this->mailer = $mailer;
        $this->queueService = $queueService;
    }

    /**
     * @param string $to
     * @param string $body
     */
    public function sendQueue(string $to, string $body)
    {
        $this->queueService->getSender()->publish(['to' => $to, 'body' => $body], QueueName::SEND_EMAIL);
    }

    /**
     * @param string $to
     * @param string $body
     * @return bool
     */
    public function send(string $to, string $body): bool
    {
        $message = $this->mailer->getMessage();
        $message->setBody($body);
        $message->setTo([$to]);
        $message->setFrom(['system@test.test' => "System"]);
        if (!!$this->mailer->getMailer()->send($message)) {
            MessageLogger::success('Email sent');
            return true;
        }
        return false;
    }
}