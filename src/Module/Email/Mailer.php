<?php

namespace src\Module\Email;

use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class Mailer
{
    /** @var Swift_SmtpTransport */
    private Swift_SmtpTransport $transport;

    /** @var Swift_Mailer */
    private ?Swift_Mailer $mailer = null;

    /** @var Swift_Message */
    private ?Swift_Message $message = null;

    public function __construct()
    {
        $this->init();
    }

    /**
     * @return Swift_Mailer
     */
    public function getMailer(): Swift_Mailer
    {
        if (!$this->mailer) {
            $this->mailer = new Swift_Mailer($this->transport);
        }
        return $this->mailer;
    }

    /**
     * @return Swift_Message
     */
    public function getMessage(): Swift_Message
    {
        if (!$this->message) {
            $this->message = new Swift_Message();
        }
        return $this->message;
    }

    private function init(): void
    {
        $this->transport = new Swift_SmtpTransport(getenv('MAILHOG_HOST'), getenv('MAILHOG_PORT'));
    }
}