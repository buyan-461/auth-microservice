BEGIN;

CREATE TABLE "user" (
	"id" SERIAL PRIMARY KEY,
	"password" VARCHAR(80) NOT NULL,
	"username" VARCHAR(80) NOT NULL UNIQUE,
	"email" VARCHAR(80)
);

CREATE TABLE "user_token" (
	"id" SERIAL PRIMARY KEY,
	"token" VARCHAR(80) NOT NULL,
	"user_id" INTEGER NOT NULL REFERENCES "user"(id) ON DELETE CASCADE,
	"expired_at" TIMESTAMP
);

CREATE INDEX "token" ON "user_token" ("token");

COMMIT;