<?php
return [
    '/' => 'auth/index',
    '/login' => 'auth/index',
    '/logout' => 'auth/logout',
    '/register' => 'auth/register',
];