<?php

use src\Core\Container\Container;
use src\Core\Queue\QueueName;
use src\Core\Queue\QueueService;

require __DIR__ . "/config/bootstrap.php";

$container = new Container();
/** @var QueueService $queueService */
$queueService = $container->get(QueueService::class);
$receiver = $queueService->getReceiver();
$receiver->listen(QueueName::SEND_EMAIL);